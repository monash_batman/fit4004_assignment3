# Test Strategy

## Introduction

The purpose of this test plan is to outline the strategy or strategies used for selecting test cases which will be used to test the functional correctness of the Strava Python application.
<br/>Unit tests was created for all the components in the application using equivalence class partitioning and boundary value 
analysis.

## Parse Args
To test the argument parser we test `parse_args()` in main.py. 
<br/>This parses the cmd line arguments outlined in the specification.

Test file: test_input_validation.py

| Argument | Test cases | 
|----------| ------------------- |
| -a       | Args is a valid float|
| -g | Valid and Invalid Gender Preference | 
| -n | Value in range of 1 <= 20, Value above upperbound, Value below lowerbound | 
| -o | String stored correctly | 
|Default Option| Default options for -g, -n and -o is correct|

| Method | Test cases | 
| --- | --- |  
| `check_search_area_boundary()` | Latitude above upperbound limit, Latitude below lowerbound limit, <br/>Longitude above upperbound limit, Longitude below lowerbound limit, Both values within boundary limits| 

## Generate Segment List
To test the generation of segment list we test `generate_sorted_segment()` in main.py. 
<br/>This creates a list of fastest times from each segment between the two points, sorting from largest to smallest.

Test file: test_main_validation.py

| Method | Test cases | 
| --- | --- |  
| `generate_sorted_segment()` | Empty segment list, HTTP Error, Outputs list in descending order | 

## Generate PDF
To test the generation of segment list we test `generate_pdf()` in main.py. 
<br/>This outputs a bar graph in a pdf file.
<br/>Assume that all parameters to this method are valid since the error handling is handled by the argument parser.

Test file: test_main_validation.py

| Method | Test cases | 
| --- | --- |  
| `generate_sorted_segment()` | Ouputs correct filename that is pdf | 


## The API Query
To test the API Query we need to test the following methods in the class Strava.py.
- `get_all_segments()`
- `get_fastest_time_in_segment()`
<br/>Assume that all parameters to these methods are valid since the error handling is handled by the argument parser

Test file: test_strava_validation.py

| Method | Test cases | 
| --- | --- |  
| `get_all_segments()` | Invalid Access Key, HTTP Error, Valid Segment, Empty Segment list | 
| `get_fastest_time_in_segment()` | Invalid Access Key, HTTP Error, Valid Fastest Time, Empty Segment list | 

## Graph
To test the Graph class we test the following attributes and methods in Graph.py

Test file: test_graph_validation.py

| Attribute | Test cases | 
| --- | --- |  
| `x` | Exist |
| `y` | Exist |
        
| Method | Test cases | 
| --- | --- |  
| `save_to_pdf()` | Ouputs correct filename that is pdf | 

## Segment
To test the Segment class we test the following attributes and methods in Segment.py

Test file: test_segment_validation.py

| Attribute | Test cases | 
| --- | --- |  
| `id` | Exist |
| `name` | Exist |
| `fastest_record` | Exist |
        
| Method | Test cases | 
| --- | --- |  
| `get_id()` | Return id |
| `get_name()` | Return name |
| `set_fastest_record` | Valid value |
| `get_fastest_record` | Return value |