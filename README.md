# fit4004_assignment3

## Algorithm followed

![Screenshot](./Algorithm.png)

## Requirements

* Python version 3.6.5

## Install dependencies

```bash
pip3 install pipenv
pipenv install
```

## Run the software

```bash
export PYTHONPATH=.
pipenv run python src/main.py [-h] -a lat1 long1 lat2 long2 -k accessKey [-g {M,F}]
               [-n {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}]
               [-o O]
```
## Usage

```bash 
main.py [-h] -a lat1 long1 lat2 long2 -k accessKey [-g {M,F}]
               [-n {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}]
               [-o O]

optional arguments:
  -h, --help            show this help message and exit
  -g {M,F}              This optional command-line argument filters the search
                        by gender. If not specified use the fastest times
                        regardless of gender.
  -n {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}
                        Display this number of segments. If not specified,
                        default to 5.
  -o O                  The name of the file to save the chart in. By default,
                        save to out.pdf (in PDF format) in the current
                        directory.

required arguments:
  -a lat1 long1 lat2 long2
                        Specify the boundary of the search area.
  -k accessKey          Specify Strava API access key
```

## Test

```bash
export PYTHONPATH=..
cd tests/
pipenv run py.test --cov=../src .
```

## Test and code coverage report

![Screenshot](./Test_Coverage_Report.png)

