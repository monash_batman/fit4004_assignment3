class Segment:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.fastest_record = None

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def set_fastest_record(self, record):
        self.fastest_record = record

    def get_fastest_record(self):
        return self.fastest_record
