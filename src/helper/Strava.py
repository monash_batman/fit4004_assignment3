import requests
from src.model.Segment import Segment


class Strava:
    API_ENDPOINT = "https://www.strava.com/api/v3"

    def __init__(self, access_key):
        self.access_key = access_key

    def get_all_segments(self, lat1, lng1, lat2, lng2):
        payload = {'bounds': '[' + str(lat1) + ',' + str(lng1) + ',' + str(lat2) + ',' + str(lng2) + ']'}
        r = requests.get(Strava.API_ENDPOINT + '/segments/explore', params=payload,
                         headers=self.get_auth_header())
        r.raise_for_status()
        segments_json = r.json()["segments"]
        if len(segments_json) == 0:
            raise ValueError("No segments found in the boundary")
        segments = []
        for segment_json in segments_json:
            segment = Segment(segment_json["id"], segment_json["name"])
            segments.append(segment)
        return segments

    def get_fastest_time_in_segment(self, segment, gender_pref):
        payload = {}
        if gender_pref != "N":
            payload = {"gender": gender_pref}
        r = requests.get(Strava.API_ENDPOINT + '/segments/' + str(segment.get_id()) + '/leaderboard',
                         headers=self.get_auth_header(), params=payload)
        r.raise_for_status()
        records_json = r.json()["entries"]
        if len(records_json) == 0:
            raise ValueError("No records found in " + str(segment.get_id()) + " segment")
        fastest_time = records_json[0]["elapsed_time"]
        for record_json in records_json[1:]:
            if fastest_time > record_json["elapsed_time"]:
                fastest_time = record_json["elapsed_time"]
        return fastest_time

    def get_auth_header(self):
        return {'authorization': 'Bearer ' + self.access_key}
