import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt


class Graph:
    def __init__(self, x, y, gender_pref):
        self.x = x
        self.y = y
        self.figure = plt
        self.gender_pref = gender_pref

    def plot_figure(self):
        # Drawing bar graph
        self.figure.bar(self.x, self.y, align='center', alpha=0.5)
        # rotating x-axis labels to be vertical
        self.figure.tick_params(axis='x', rotation=90)
        self.figure.ylabel('Time (seconds)')
        self.figure.title(
            'Strava - ' + str(len(self.x)) + ' slowest fastest segments ranking ' + '(' + self.gender_pref + ')')
        if len(self.x) == 0:
            self.figure.title("Strava - No data")
        self.figure.draw()

    def save_to_pdf(self, output_file_name):
        self.figure.savefig(output_file_name, bbox_inches='tight')
