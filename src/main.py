import sys
import argparse
import os
from src.helper.Strava import Strava
from src.helper.Graph import Graph
from requests import HTTPError

def check_search_area_boundary(locations):
    args_description = ["lat1", "long1", "lat2", "long2"]
    for i in range(0, len(locations)):
        if not i % 2 == 0:
            if not (-180 <= locations[i] <= 180):
                raise ValueError(
                    os.path.basename(sys.argv[0]) + ": error: argument -a: invalid value " + args_description[
                        i] + " should be within +-180")
        else:
            if not (-90 <= locations[i] <= 90):
                raise ValueError(
                    os.path.basename(sys.argv[0]) + ": error: argument -a: invalid value " + args_description[
                        i] + " should be within +-90")


def parse_args(args):
    parser = argparse.ArgumentParser()
    req_args = parser.add_argument_group(title='required arguments')
    req_args.add_argument("-a", required="True", metavar=("lat1", "long1", "lat2", "long2"), type=float, nargs=4,
                          help="Specify the boundary of the search area.")

    req_args.add_argument("-k", required="True", metavar="accessKey", type=str, help="Specify Strava API access key")

    parser.add_argument("-g", type=str, default="N",
                        help="This optional command-line argument filters the search by gender. If not specified use the fastest times regardless of gender.",
                        choices=("M", "F"))
    parser.add_argument("-n", type=int, default=5,
                        help="Display this number of segments. If not specified, default to 5.",
                        choices=range(1, 21))
    parser.add_argument("-o", type=str, default="out.pdf",
                        help="The name of the file to save the chart in. By default, save to out.pdf (in PDF format) in the current directory.")

    args = parser.parse_args(args)
    try:
        check_search_area_boundary(args.a)
    except ValueError as e:
        parser.print_usage(sys.stderr)
        print(str(e), file=sys.stderr)
        sys.exit(2)
    return args


def generate_sorted_segment(args):
    starva = Strava(args.k)
    locations = args.a
    try:
        segment_list = starva.get_all_segments(locations[0], locations[1], locations[2], locations[3])
    except HTTPError as e:
        print(str(e), file=sys.stderr)
        sys.exit(2)
    except ValueError as e:
        print("Warning: " + str(e))
        segment_list = []

    for segment in segment_list:
        try:
            segment.set_fastest_record(starva.get_fastest_time_in_segment(segment, args.g))
        except HTTPError as e:
            print(str(e), file=sys.stderr)
            sys.exit(2)
        except ValueError as e:
            print("Warning: " + str(e))
            segment.set_fastest_record(-1)
    segment_list.sort(key=lambda x: x.get_fastest_record(), reverse=True)
    return segment_list


def generate_pdf(segment_list, output_file_name, gender_pref):
    # Y-axis values
    segment_time = []
    # X-axis values
    segment_names = []
    for segment in segment_list:
        segment_names.append(str(segment.get_name()))
        segment_time.append(int(segment.get_fastest_record()))

    if gender_pref == "N":
        gender_pref = "All"
    elif gender_pref == "F":
        gender_pref = "Female"
    elif gender_pref == "M":
        gender_pref = "Male"

    graph = Graph(segment_names, segment_time, gender_pref)
    graph.plot_figure()
    graph.save_to_pdf(output_file_name)


def main():
    args = parse_args(sys.argv[1:])
    segments = generate_sorted_segment(args)
    generate_pdf(segments[:args.n], args.o, args.g)


if __name__ == '__main__':
    main()
