import unittest
from src.helper.Graph import Graph
import os


class TestGraphValidation(unittest.TestCase):
    def setUp(self):
        self.graph = Graph(
            ['Mount Lemmon', 'Palomar Mt South Grade (Taco shop to Summit Stop Sign)', 'San Bruno Hill Climb',
             'Chantry Flat from the Gate', 'Old La Honda - Mile 1'], [4738, 3075, 868, 763, 236], "N")

    def test_init(self):
        # Test all init values are correct
        self.assertListEqual(self.graph.x, ['Mount Lemmon', 'Palomar Mt South Grade (Taco shop to Summit Stop Sign)',
                                            'San Bruno Hill Climb',
                                            'Chantry Flat from the Gate', 'Old La Honda - Mile 1'])
        self.assertListEqual(self.graph.y, [4738, 3075, 868, 763, 236])

        # Test if the graph api save the file with customised file name
    def test_save_to_pdf(self):
        self.graph.plot_figure()
        self.graph.save_to_pdf("test_graph_validation.pdf")
        self.assertTrue(os.path.isfile("test_graph_validation.pdf"))
        os.remove("test_graph_validation.pdf")


if __name__ == '__main__':
    unittest.main()
