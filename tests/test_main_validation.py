import unittest
from src.main import generate_sorted_segment
from src.main import generate_pdf
from unittest import mock
from requests import HTTPError
from src.model.Segment import Segment
import os


class MockArgs:
    def __init__(self, a, k, g):
        self.a = a
        self.k = k
        self.g = g


class TestMainValidation(unittest.TestCase):
    # Test the functions available in main.py
    def setUp(self):
        self.segment_list = [Segment(527881, "Mount Lemmon"),
                             Segment(273807, "Palomar Mt South Grade (Taco shop to Summit Stop Sign)"),
                             Segment(378701, "San Bruno Hill Climb")]

    def test_generate_sorted_segment_empty_segment(self):
        # Test how it reacts with empty segment response
        def get_all_segments(lat1, lng1, lat2, lng2):
            raise ValueError("No segments found in the boundary")

        def get_fastest_time_in_segment(segment, gender_pref):
            raise ValueError("No records found in " + str(segment.get_id()) + " segment")

        with mock.patch('src.helper.Strava.Strava.get_all_segments', side_effect=get_all_segments), mock.patch(
                'src.helper.Strava.Strava.get_fastest_time_in_segment', side_effect=get_fastest_time_in_segment):
            segment_list = generate_sorted_segment(
                MockArgs([37.8331119, -122.4834356, 37.8280722, -122.4981393], "validKey", "N"))
            self.assertListEqual(segment_list, [])

    def test_generate_sorted_segment_error_network_code(self):
        # Test how it reacts with http error response
        def get_all_segments(lat1, lng1, lat2, lng2):
            raise HTTPError("401 Client Error")

        with mock.patch('src.helper.Strava.Strava.get_all_segments', side_effect=get_all_segments):
            with self.assertRaises(SystemExit):
                segment_list = generate_sorted_segment(
                    MockArgs([37.8331119, -122.4834356, 37.8280722, -122.4981393], "validKey", "N"))

    def test_generate_sorted_segment(self):
        # Test if it outputs sorted value in descending order
        def get_all_segments(lat1, lng1, lat2, lng2):
            return self.segment_list

        def get_fastest_time_in_segment(segment, gender_pref):
            return segment.get_id()

        with mock.patch('src.helper.Strava.Strava.get_all_segments', side_effect=get_all_segments), mock.patch(
                'src.helper.Strava.Strava.get_fastest_time_in_segment', side_effect=get_fastest_time_in_segment):
            segment_list = generate_sorted_segment(
                MockArgs([37.8331119, -122.4834356, 37.8280722, -122.4981393], "validKey", "N"))
            for i in range(1, len(segment_list)):
                self.assertGreater(segment_list[i - 1].get_fastest_record(), segment_list[i].get_fastest_record())

    def test_generate_pdf_output_file_name(self):
        # Test if it outputs pdf file
        def get_all_segments(lat1, lng1, lat2, lng2):
            return self.segment_list

        def get_fastest_time_in_segment(segment, gender_pref):
            return segment.get_id()

        with mock.patch('src.helper.Strava.Strava.get_all_segments', side_effect=get_all_segments), mock.patch(
                'src.helper.Strava.Strava.get_fastest_time_in_segment', side_effect=get_fastest_time_in_segment):
            segment_list = generate_sorted_segment(
                MockArgs([37.8331119, -122.4834356, 37.8280722, -122.4981393], "validKey", "N"))
        generate_pdf(segment_list, "test_main_validation.pdf", "N")
        self.assertTrue(os.path.isfile("test_main_validation.pdf"))
        os.remove("test_main_validation.pdf")


if __name__ == '__main__':
    unittest.main()
