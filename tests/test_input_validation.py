import unittest
from src.main import parse_args


class TestInputValidation(unittest.TestCase):
    def test_no_required_args(self):
        # Test missing one required args
        with self.assertRaises(SystemExit):
            parse_args(["-a", "91", "160", "90", "160"])

    def test_search_area_string_value(self):
        # Test if -a rejects string value
        with self.assertRaises(SystemExit):
            parse_args(["-a", "a", "160", "90", "160", "-k", "testKey"])

    def test_search_area_lat_upper_bound(self):
        # Test -a lat arg upper bound
        with self.assertRaises(SystemExit):
            parse_args(["-a", "91", "160", "90", "160", "-k", "testKey"])

    def test_search_area_lat_lower_bound(self):
        # Test -a lat arg lower bound
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "160", "-91", "160", "-k", "testKey"])

    def test_search_area_lng_upper_bound(self):
        # Test -a lng arg upper bound
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "181", "-90", "160", "-k", "testKey"])

    def test_search_area_lng_lower_bound(self):
        # Test -a lng arg lower bound
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "180", "-90", "-181", "-k", "testKey"])

    def test_search_area_within_bound(self):
        # Test -a within bound
        args = parse_args(["-a", "-90", "180", "90", "-180", "-k", "testKey"])
        locations = args.a
        self.assertEqual(locations[0], -90)
        self.assertEqual(locations[1], 180)
        self.assertEqual(locations[2], 90)
        self.assertEqual(locations[3], -180)

    def test_invalid_gender_pref(self):
        # Test -g invalid gender selection
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "181", "-90", "160", "-k", "testKey", "-g", "TEST"])

    def test_valid_gender_pref(self):
        # Test -g valid gender selection
        args = parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey", "-g", "M"])
        gender_pref = args.g
        self.assertEqual(gender_pref, "M")

    def test_number_of_segments_upper_bound(self):
        # Test -n number of segments upper bound
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey", "-n", "21"])

    def test_number_of_segments_lower_bound(self):
        # Test -n number of segments lower bound
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey", "-n", "0"])

    def test_number_of_segments_string_value(self):
        # Test if -n rejects string value
        with self.assertRaises(SystemExit):
            parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey", "-n", "test"])

    def test_number_of_segments_within_bound(self):
        # Test -n number of segments within bound
        args = parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey", "-n", "5"])
        segment_limits = args.n
        self.assertEqual(segment_limits, 5)

    def test_custom_output_filename(self):
        # Test -o custom filename
        args = parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey", "-o", "test"])
        custom_output_filename = args.o
        self.assertEqual(custom_output_filename, "test")

    def test_default_options(self):
        # Test -o default filename
        args = parse_args(["-a", "-90", "180", "-90", "160", "-k", "testKey"])
        self.assertEqual(args.g, "N")
        self.assertEqual(args.n, 5)
        self.assertEqual(args.o, "out.pdf")


if __name__ == '__main__':
    unittest.main()
