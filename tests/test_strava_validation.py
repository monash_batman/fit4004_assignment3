from src.helper.Strava import Strava
import unittest
from requests import HTTPError
from unittest import mock
from src.model.Segment import Segment
import json


class MockResponse:
    def __init__(self, json_data, status_code, url):
        self.json_data = json_data
        self.status_code = status_code
        self.url = url

    def json(self):  # Mock requests json function
        return self.json_data

    def raise_for_status(self):  # Mock requests raise_for_status function
        if self.status_code == 200:
            return
        elif self.status_code == 400:
            raise HTTPError("400 Client Error: Bad Request for url: " + self.url)
        elif self.status_code == 401:
            raise HTTPError("401 Client Error: Unauthorized for url: " + self.url)
        elif self.status_code == 503:
            raise ConnectionError("Request timeout")


class TestStravaValidation(unittest.TestCase):
    def test_invalid_access_key_get_all_segments(self):
        # Test how get_all_segments method reacts to 401 unauthorised response
        def mocked_get_request(*args, **kwargs):
            return MockResponse({
                "message": "Authorization Error",
                "errors": [
                    {
                        "resource": "Application",
                        "field": "",
                        "code": "invalid"
                    }
                ]
            }, 401, args[0])

        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('invalidKey')
            with self.assertRaises(HTTPError):
                mgc.get_all_segments("-90", "180", "90", "-180")

    def test_invalid_params_get_all_segments(self):
        # Test how get_all_segments reacts to bad request response
        def mocked_get_request(*args, **kwargs):
            return MockResponse({
                "message": "Bad Request",
                "errors": [
                    {
                        "resource": "Leaderboard",
                        "field": "bounds",
                        "code": "invalid"
                    }
                ]
            }, 400, args[0])

        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('validKey')
            with self.assertRaises(HTTPError):
                mgc.get_all_segments("-90", "180", "90", "a")

    def test_get_all_segments(self):
        # Test if get_all_segments method returns correct data structure [segement, segment]
        def mocked_get_request(*args, **kwargs):
            with open("segments_data.json") as file:
                mocked_json = json.load(file)
            return MockResponse(mocked_json, 200, args[0])

        with open("segments_data.json") as file:
            data = json.load(file)["segments"]
        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('validKey')
            segment_list = mgc.get_all_segments(37.8331119, -122.4834356, 37.8280722, -122.4981393)
            for i in range(0, len(segment_list)):
                self.assertEqual(segment_list[i].get_id(), data[i]["id"])
                self.assertEqual(segment_list[i].get_name(), data[i]["name"])
                self.assertEqual(segment_list[i].get_fastest_record(), None)

    def test_get_all_segments_no_segments(self):
        # Test how get_all_segments method reacts to no segments data
        def mocked_get_request(*args, **kwargs):
            return MockResponse({
                "segments": []
            }, 200, args[0])

        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('validKey')
            with self.assertRaises(ValueError):
                segment_list = mgc.get_all_segments(37.8331119, -122.4834356, 37.8280722, -122.4834356)

    def test_invalid_access_key_fastest_time_in_segment(self):
        # Test how get_fastest_time_in_segment reacts to 401 unauthorised response
        def mocked_get_request(*args, **kwargs):
            return MockResponse({
                "message": "Authorization Error",
                "errors": [
                    {
                        "resource": "Application",
                        "field": "",
                        "code": "invalid"
                    }
                ]
            }, 401, args[0])

        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('invalidKey')
            mock_segment = Segment(527881, "Mount Lemmon")
            with self.assertRaises(HTTPError):
                mgc.get_fastest_time_in_segment(mock_segment, "N")

    def test_invalid_params_fastest_time_in_segment(self):
        # Test how get_fastest_time_in_segment reacts to 400 bad request response
        def mocked_get_request(*args, **kwargs):
            return MockResponse({
                "message": "Record Not Found",
                "errors": [
                    {
                        "resource": "Segment",
                        "field": "id",
                        "code": "invalid"
                    }
                ]
            }, 400, args[0])

        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('validKey')
            mock_segment = Segment(527881, "Mount Lemmon")
            with self.assertRaises(HTTPError):
                mgc.get_fastest_time_in_segment(mock_segment, "N")

    def test_fastest_time_in_segment(self):
        # Test if the fastest_time_in_segment methods return fastest time in the json response
        def mocked_get_request(*args, **kwargs):
            with open("leaderboard_527881.json") as file:
                mocked_json = json.load(file)
            return MockResponse(mocked_json, 200, args[0])

        with open("leaderboard_527881.json") as file:
            data = json.load(file)
        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('validKey')
            mock_segment = Segment(527881, "Mount Lemmon")
            fastest_time = mgc.get_fastest_time_in_segment(mock_segment, "N")
            self.assertEqual(fastest_time, data["entries"][0]["elapsed_time"])

    def test_fastest_time_in_segment_no_records(self):
        # Test how fastest_time_in_segment method reacts to empty entries in the response
        def mocked_get_request(*args, **kwargs):
            return MockResponse({
                "effort_count": 7022,
                "entry_count": 7022,
                "kom_type": "kom",
                "entries": []
            }, 200, args[0])

        with mock.patch('requests.get', side_effect=mocked_get_request):
            mgc = Strava('validKey')
            mock_segment = Segment(527880, "Mount Lemmon")
            with self.assertRaises(ValueError):
                mgc.get_fastest_time_in_segment(mock_segment, "N")

    def test_get_auth_header(self):
        # Test if authorization header got constructed properly
        mgc = Strava('validKey')
        self.assertDictEqual(mgc.get_auth_header(), {'authorization': 'Bearer validKey'})


if __name__ == '__main__':
    unittest.main()
