from src.model.Segment import Segment
import unittest


class TestSegmentValidation(unittest.TestCase):
    def setUp(self):
        self.segment = Segment(12345, "test")

    def test_init(self):
        # Test all init values are correct
        self.assertEqual(self.segment.id, 12345)
        self.assertEqual(self.segment.name, "test")
        self.assertIsNone(self.segment.fastest_record)

    def test_get_id(self):
        # Test get_id function
        self.assertEqual(self.segment.get_id(), 12345)

    def test_get_name(self):
        # Test get_name function
        self.assertEqual(self.segment.get_name(), "test")

    def test_fastest_record(self):
        # Test get_fastest_record function
        self.segment.set_fastest_record(12345678)
        self.assertEqual(self.segment.get_fastest_record(), 12345678)


if __name__ == '__main__':
    unittest.main()
